# AUSG(Awskrug University Student Group)🙌

<p align="center">
 <img width="300" src="https://github.com/AUSG/Seminar-List/blob/master/images/ausg_logo.png?raw=true"/>
</p>

## 2기 세미나

### 초보자를 위한 AWS 뿌시기 세미나 🤜🤛

------

### 8차 세미나 : AWS 계정을 갓 생성한 초보자를 위한 세미나

- 발표자 : [조민지](https://github.com/jominjimail)
- 날짜 : 2018.12.06
- 자료 : [GitHub Repo](https://github.com/jominjimail/ausg/tree/master/hands_on)

<p align="center">
<img width="400" src="https://github.com/AUSG/Seminar-List/blob/master/images/minji_break_1.JPG?raw=true">
<img width="400" src="https://github.com/AUSG/Seminar-List/blob/master/images/minji_break_2.JPG?raw=true">
</p>

------

### 7차 세미나 : Docker 입문자를 위한 Docker의 이해와 Jenkins/ECS 실습

- 발표자 : [김상열](https://github.com/sangyeol-kim)
- 날짜 : 2018.11.22
- 자료 : [GitHub Repo](https://github.com/sangyeol-kim/ausg-docker-seminar)

<p align="center">
<img width="400" src="https://github.com/AUSG/Seminar-List/blob/master/images/sangyeol_break_1.JPG?raw=true">
<img width="400" style="transform:rotate(90deg);" src="https://github.com/AUSG/Seminar-List/blob/master/images/sangyeol_break_2.JPG?raw=true">
</p>

------

### 6차 세미나 : Node.js 서버리스 프레임웍을 사용하여 싱글페이지 포트폴리오 제작하기

- 발표자 : [고윤호](https://github.com/yoonhoGo)
- 날짜 : 2018.09.19
- 자료 : [GitHub Repo](https://github.com/yoonhoGo/serverless-spa-portfolio)

<p align="center">
<img width="400" src="https://github.com/AUSG/seminar-list/blob/master/images/yoonho_break_1.JPG?raw=true" />
<img width="400" src="https://github.com/AUSG/Seminar-List/blob/master/images/yoonho_break_2.jpeg?raw=true" />
</p>

------

### 외부 세미나 🤗

------

### AWS Community Day re:cap : 블록체인 원장 DB를 클라우드로 QLDB

- 발표자 : [김예본](http://github.com/yebonkim)
- 날짜 : 2019.01.25
- 자료 : [SlideShare](https://www.slideshare.net/awskr/amazon-quatom-fully-managed-ledger-database)

<p align="center">
<img width="400" src="https://github.com/AUSG/Seminar-List/blob/master/images/yebon_recap_1.JPG?raw=true">
<img width="400" src="https://github.com/AUSG/Seminar-List/blob/master/images/yebon_recap_2.JPG?raw=true">
</p>

------

## 1기 세미나

### 초보자를 위한 AWS 뿌시기 세미나 🤜🤛

------

### 5차 세미나 : 자바스크립트로 트렌디한 웹개발과 쉬운 배포 경험하기

- 발표자 : [원지혁](https://github.com/jeehyukwon)
- 날짜 : 2018.02.19
- 자료 : [GitHub Repo](https://github.com/AUSG/ausg-seminar-2018/tree/master/TrendyWebDev)

<p align="center">
<img width="400" src="https://github.com/AUSG/seminar-list/blob/master/images/jeehyuk_break_1.jpg?raw=true" />
</p>

------

### 4차 세미나 : 무료 이미지 리사이즈 서비스 만들기

- 발표자 : [한만혁](https://github.com/ManHyuk)
- 날짜 : 2018.02.08
- 자료 : [GitHub Repo](https://github.com/ManHyuk/ausg-seminar-2018)

<p align="center">
<img width="400" src="https://github.com/AUSG/seminar-list/blob/master/images/manhyuk_break_1.jpg?raw=true" />
</p>

------

### 3차 세미나 : AWS로 구현하는 손쉬운 앱 로그인 with AWS Cognito

- 발표자 : [김종찬](https://github.com/acious)
- 날짜 : 2018.02.01

<p align="center">
<img width="400" src="https://github.com/AUSG/seminar-list/blob/master/images/jongchan_break_1.jpeg?raw=true" />
<img width="400" src="https://github.com/AUSG/seminar-list/blob/master/images/jongchan_break_2.jpeg?raw=true" />
</p>

------

### 2차 세미나 : 팀플 과제를 여행하는 히치하이커를 위한 안내서 with AWS C9, AWS Elastic Beanstalk

- 발표자 : [유호균](https://github.com/philographer)
- 날짜 : 2018.01.25
- 자료 : [GitHub Repo](https://github.com/awskrug/ausg-seminar-2018/tree/master/teamplay) 

<p align="center">
<img width="400" src="https://github.com/AUSG/seminar-list/blob/master/images/hogyun_break_1.jpeg?raw=true" />
<img width="400" src="https://github.com/AUSG/seminar-list/blob/master/images/hogyun_break_2.jpeg?raw=true" />
</p>

------

### 1차 세미나 : Python으로 비트코인 챗봇 만들기 가즈아ㅏㅏ with AWS EC2, Django

- 발표자 : [김현중](https://github.com/Exubient)
- 날짜 : 2018.01.11
- 자료 : [GitHub Repo](https://github.com/Exubient/AUSG_KakaoBot)

<p align="center">
<img width="400" src="https://github.com/AUSG/Seminar-List/blob/master/images/hyunjoong_break_1.jpeg?raw=true" />
<img width="400" src="https://github.com/AUSG/Seminar-List/blob/master/images/hyunjoong_break_2.jpeg?raw=true" />
</p>

------

### 외부 세미나 🤗

------

### AWS Community Day re:cap : SageMaker로 강화학습(RL) 마스터링

- 발표자 : [남궁선](https://github.com/whitesoil)
- 날짜 : 2019.01.25
- 자료 : [SlideShare](https://www.slideshare.net/mobile/awskr/tag/awscommunityday2019)

  <p align="center">
  <img width="400" src="https://github.com/AUSG/seminar-list/blob/master/images/seon_recap_1.jpg?raw=true" />
  <img width="400" src="https://github.com/AUSG/seminar-list/blob/master/images/seon_recap_2.JPG?raw=true" />
  </p> 

------

### AWS Community Day APAC : MyMusicTaste에서 Serverless DataLake 구축 사례, AUSG 소개

- 발표자 : [김현중](https://github.com/Exubient)
- 날짜 : 2018.09.17
- 자료 : [발표자료](https://docs.google.com/presentation/d/1qaDSkaFinTg24nqpNZRQg5GciA0zoJ49jNgXF2Q6nNI/edit?usp=sharing)

  <p align="center">
  <img width="400" src="https://github.com/AUSG/seminar-list/blob/master/images/hyunjoong_vietnam_2.JPG?raw=true" />
  <img width="400" src="https://github.com/AUSG/seminar-list/blob/master/images/hyunjoong_vietnam_3.JPG?raw=true" />
  </p> 

------

### 고려대학교 해커톤 세미나

- 발표자 : [윤서현](https://github.com/seohyun0120)
- 날짜 : 2018.08.11

------

### Amathon 사전 세션 : EC2 + S3 + RDS를 사용해 Django 웹 서버 구축하기

- 발표자 : [김현중](https://github.com/Exubient)
- 날짜 : 2018.07.19
- 자료 : [GitHub Repo](https://github.com/AUSG/amathon-django)

  <p align="center">
  <img width="400" src="https://github.com/AUSG/seminar-list/blob/master/images/hyunjoong_amathon_1.jpg?raw=true" />
  </p> 

### Amathon 사전 세션 : Amazon SageMaker를 활용하여 MNIST 머신러닝 맛보기

- 발표자 : [남궁선](https://github.com/whitesoil)
- 날짜 : 2018.07.19
- 자료 : [GitHub Repo](https://github.com/AUSG/amathon-sagemaker)

  <p align="center">
  <img width="400" src="https://github.com/AUSG/seminar-list/blob/master/images/seon_amathon_1.jpg?raw=true" />
  </p> 

### Amathon 사전 세션 : Serverless Framework를 활용한 SPA 배포 실습

- 발표자 : [원지혁](http://github.com/jeehyukwon)
- 날짜 : 2018.07.19
- 자료 : [GitHub Repo](https://github.com/AUSG/amathon-sls-spa)

  <p align="center">
  <img width="400" src="https://github.com/AUSG/seminar-list/blob/master/images/jeehyuk_amathon_1.jpg?raw=true" />
  </p> 

------

### AUSG 세미나 For 멋쟁이 사자처럼 : 인스타그램처럼 사진 업로드 해보기

- 발표자 : [원지혁](http://github.com/jeehyukwon)
- 날짜 : 2018.05.14
- 자료 : [GitHub Repo](https://github.com/ausg-likelion-seminar)

  <p align="center">
  <img width="400" src="https://github.com/AUSG/seminar-list/blob/master/images/jeehyuk_likelion_1.png?raw=true" />
  <img width="400" src="https://github.com/AUSG/seminar-list/blob/master/images/jeehyuk_likelion_2.png?raw=true" />
  </p> 

### AUSG 세미나 For 멋쟁이 사자처럼 : AUSG 소개

- 발표자 : [남궁선](https://github.com/whitesoil)
- 날짜 : 2018.05.14

------

### 삼성 SDS 세미나 : REST의 고통은 이제 그만, 차근차근 GraphQL 시작하기

- 발표자 : [원지혁](http://github.com/jeehyukwon)
- 날짜 : 2018.05.02
- 자료 : [Youtube 링크](https://www.youtube.com/watch?v=-qtPfRSxwXA)

<p align="center">
<img width="400" src="https://github.com/AUSG/seminar-list/blob/master/images/jeehyuk_sds_1.jpeg?raw=true" />
<img width="400" src="https://github.com/AUSG/seminar-list/blob/master/images/jeehyuk_sds_2.jpeg?raw=true" />
</p>

------

### AWS Summit Seoul 2018 : Serverless로 이미지 프로토타입 개발기

- 발표자 : [유호균](https://github.com/philographer)
- 날짜 : 2018.04.18

<p align="center">
<img width="400" src="https://github.com/AUSG/seminar-list/blob/master/images/hogyun_summit_1.jpg?raw=true" />
<img width="400" src="https://github.com/AUSG/seminar-list/blob/master/images/hogyun_summit_2.jpg?raw=true" />
</p>

------

### 한양대 Erica 게릴라 세미나 : AWS를 사용하여 웹페이지를 서버에 업로드하고 도메인을 등록해 웹페이지를 서비스해보기

- 발표자 : [남궁선](https://github.com/whitesoil)
- 날짜 : 2018.04.10
- 자료 : [GitHub Repo](https://github.com/whitesoil/AUSG-LikeLion)

<p align="center">
<img width="400" src="https://github.com/AUSG/seminar-list/blob/master/images/seon_hanyang_guerrilla_1.JPG?raw=true" />
<img width="400" src="https://github.com/AUSG/seminar-list/blob/master/images/seon_hanyang_guerrilla_2.JPG?raw=true" />
</p>

------

### 한양대 창업동아리 박람회 : AUSG 소개

- 발표자 : [남궁선](https://github.com/whitesoil)
- 날짜 : 2018.03.29
- 자료 : [발표자료](https://docs.google.com/file/d/1264IGn-Hy_Igv7UOmlHd1MsOK9E3UKq_/edit?usp=docslist_api&filetype=mspresentation)

<p align="center">
<img width="400" src="https://github.com/AUSG/seminar-list/blob/master/images/seon_startup_1.png?raw=true" />
<img width="400" src="https://github.com/AUSG/seminar-list/blob/master/images/seon_startup_2.png?raw=true" />
</p>

### 한양대 창업동아리 박람회 : Amazon Web Services 클라우드 소개와 사례

- 발표자 : 박소정
- 날짜 : 2018.03.29

<p align="center">
<img width="400" src="https://github.com/AUSG/seminar-list/blob/master/images/sojeong_startup_1.png?raw=true" />
<img width="400" src="https://github.com/AUSG/seminar-list/blob/master/images/sojeong_startup_2.png?raw=true" />
</p>

------

### Unithon Seminar : Wildrydes 서버리스 웹 애플리케이션 워크샵

- 발표자 : [유호균](https://github.com/philographer)
- 날짜 : 2018.01.26
- 자료 : [GitHub Repo](https://github.com/AUSG/unithon-serverless-2018)

------

### Community Day 2018 : App Sync, 모바일 개발을 위한 GraphQL as a Service

- 발표자 : [원지혁](http://github.com/jeehyukwon)
- 날짜 : 2018.01.20
- 자료 : [Youtube 링크](https://www.youtube.com/watch?v=chUsMVg04nU)

<p align="center">
<img width="400" src="https://github.com/AUSG/seminar-list/blob/master/images/jeehyuk_communityday_1.jpg?raw=true" />
<img width="400" src="https://github.com/AUSG/seminar-list/blob/master/images/jeehyuk_communityday_2.jpg?raw=true" />
</p>

------
